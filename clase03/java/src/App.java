import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        ejercicio7();
    }

    public static void ejercicio4(){
        System.out.println("ingrese velocidad en kilometros por hora: ");
        Scanner velocidad = new Scanner(System.in);
        double velkmh = velocidad.nextInt();
        double velms = velkmh / 3.6;
        System.out.println("la velocidad en metros por segundo es:  " + velms);
        velocidad.close();
    }

    public static void ejercicio5 (){
        System.out.println("ingrese cateto opuesto: ");
        Scanner catetos = new Scanner(System.in);
        double co = catetos.nextInt();
        System.out.println("ingrese cateto adyacente: ");
        double cad = catetos.nextInt();
        catetos.close();
        double hipotenuza = Math.sqrt((Math.pow(co, 2) + Math.pow(cad, 2)));
        System.out.println("la hipotenuza es: " + hipotenuza);
    }

    public static void ejercicio6(){
        System.out.println(" ingrese numero: ");
        Scanner num = new Scanner(System.in);
        int numero = num.nextInt();
        if (numero%10 == 0){
            System.out.println("el numero " + numero +" es multiplo de 10");
        }else if(numero%10 != 0){
            System.out.println("el numero "+ numero + " no es multiplo de 10");
        }
        num.close();
    }

    public static void ejercicio7(){
        try {
            System.out.println("ingrese una letra: ");
            Scanner x = new Scanner(System.in);
            char letra = x.next().charAt(0);       
            String letra01 = (Character.isUpperCase(letra)) ? "El caracter ingresado es una mayuscula": "El caracter ingresado no es una mayuscula";
            System.out.println(letra01);
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("hay un error en el condicional");
            }
    }


    public static void ejercicio8(){
        try {
            System.out.println("ingrese un numero: ");
            Scanner x = new Scanner(System.in);
            int num_1 = x.nextInt();
            System.out.println("ingrese un segundo numero: ");
            int num_2 = x.nextInt();
            double resultado = num_1/num_2;
            System.out.println("el resultado de la divicion es: "+ resultado);
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("ocurrio un error division por cero ");
        }
    }

    public static void ejercicio9(){
        System.out.println("ingrese un numero: ");
            Scanner x = new Scanner(System.in);
            int num_1 = x.nextInt();
            System.out.println("ingrese un segundo numero: ");
            int num_2 = x.nextInt();
            System.out.println("ingrese un tercer numero: ");
            int num_3 = x.nextInt();
            if ((num_1>num_2) && (num_1 > num_3)){
                System.out.println("el numero mayor es el: "+ num_1);
            }else if ((num_2>num_1) && (num_2 > num_3)){
                System.out.println("el numero mayor es el: "+ num_2);
            }else if ((num_3>num_2) && (num_3 > num_1)){
                System.out.println("el numero mayor es el: "+ num_3);
            }
            x.close();
    }

}
